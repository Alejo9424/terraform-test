variable "access_key" {
  type = string
}

variable "secret_key" {
  type = string
}
variable "AWS_REGION" {
  type        = string
  default     = "us-east-1"
  description = "Region services AWS"
}

locals {
  aws_region     = "us-east-1"
  aws_account_id = "270044560525"
}

locals {
  aws_kms_s3_encryption_key_arn                  = "arn:aws:kms:${local.aws_region}:${local.aws_account_id}:alias/aws/s3"
  aws_codepipeline_artifact_store_s3_bucket_name = "codepipeline-${local.aws_region}-795346648858"
  aws_codestar_connection_arn                    = "arn:aws:codestar-connections:${local.aws_region}:${local.aws_account_id}:connection/934fc49c-3795-4758-8f29-1866068aaf75"
}