# resource "aws_ecs_service" "lumen_prueba" {
#   cluster                            = aws_ecs_cluster.testing.id
#   deployment_maximum_percent         = 200
#   deployment_minimum_healthy_percent = 100
#   desired_count                      = 1
#   health_check_grace_period_seconds  = 120
#   launch_type                        = "FARGATE"
#   name                               = "lumen-prueba-service"
#   platform_version                   = "LATEST"
#   scheduling_strategy                = "REPLICA"
#   enable_ecs_managed_tags            = true

#   task_definition = join(":", [
#     aws_ecs_task_definition.lumen_prueba.family,
#     max(aws_ecs_task_definition.lumen_prueba.revision, data.aws_ecs_task_definition.lumen_prueba.revision),
#   ])

#   deployment_controller {
#     type = "ECS"
#   }

#   load_balancer {
#     container_name   = "test"
#     container_port   = 80
#     target_group_arn = aws_lb_target_group.lumen_prueba.arn
#   }

#   network_configuration {
#     assign_public_ip = true

#     security_groups = [
#       aws_security_group.testing.id,
#     ]

#     subnets = [
#       aws_subnet.test-subnet-public-1a.id
#     ]
#   }

#   lifecycle {
#     ignore_changes = [desired_count]
#   }

#   depends_on = [
#     aws_lb.lumen_test
#   ]
# }
