# resource "aws_internet_gateway" "test-igw" {
#   vpc_id = aws_vpc.test-vpc.id

#   tags = {
#     Name      = "test-ig",
#     Terraform = "true"
#   }
# }

# resource "aws_route_table" "test-public-crt" {
#   vpc_id = aws_vpc.test-vpc.id
#   route {
#     //associated subnet can reach everywhere
#     cidr_block = "0.0.0.0/0"
#     //CRT uses this IGW to reach internet
#     gateway_id = aws_internet_gateway.test-igw.id
#   }

#   tags = {
#     Name      = "test-public-crt",
#     Terraform = "true"
#   }
# }

# resource "aws_route" "test_igw" {
#   route_table_id         = aws_route_table.test-public-crt.id
#   destination_cidr_block = "0.0.0.0/0"
#   gateway_id             = aws_internet_gateway.test-igw.id
# }

# resource "aws_route_table_association" "test-crta-public-subnet-1a" {
#   subnet_id      = aws_subnet.test-subnet-public-1a.id
#   route_table_id = aws_route_table.test-public-crt.id
# }

# resource "aws_vpc_endpoint" "dkr" {
#   vpc_id              = aws_vpc.test-vpc.id
#   private_dns_enabled = true
#   service_name        = "com.amazonaws.${local.aws_region}.ecr.dkr"
#   vpc_endpoint_type   = "Interface"
#   security_group_ids = [
#     aws_security_group.testing.id,
#   ]
#   subnet_ids = [
#     aws_subnet.test-subnet-private-1a.id,
#     aws_subnet.test-subnet-private-1b.id
#   ]

#   tags = {
#     Name      = "dkr-endpoint"
#     Terraform = "true"
#   }
# }


# resource "aws_route_table_association" "test_private_1" {
#   subnet_id      = aws_subnet.test-subnet-private-1a.id
#   route_table_id = aws_route_table.test-public-crt.id
# }

# resource "aws_route_table_association" "test_private_2" {
#   subnet_id      = aws_subnet.test-subnet-private-1b.id
#   route_table_id = aws_route_table.test-public-crt.id
# }