# resource "aws_security_group" "testing" {
#   name        = "testing"
#   description = "default VPC security group"
#   vpc_id      = aws_vpc.test-vpc.id

#   tags = {
#     Name      = "testing-default-sg"
#     Terraform = "true"
#   }

#   ingress {
#     description = ""
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     self        = true
#   }

#   ingress {
#     description = "HTTP"
#     from_port   = 80
#     to_port     = 80
#     protocol    = "tcp"
#     cidr_blocks = [
#       "0.0.0.0/0",
#     ]
#     ipv6_cidr_blocks = [
#       "::/0",
#     ]
#   }

#   ingress {
#     description = "HTTPS"
#     from_port   = 443
#     to_port     = 443
#     protocol    = "tcp"
#     cidr_blocks = [
#       "0.0.0.0/0",
#     ]
#     ipv6_cidr_blocks = [
#       "::/0",
#     ]
#   }

#   egress {
#     description = ""
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = [
#       "0.0.0.0/0",
#     ]
#   }
# }