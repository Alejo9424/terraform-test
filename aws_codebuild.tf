# resource "aws_codebuild_project" "test" {
#   badge_enabled  = false
#   build_timeout  = 60
#   description    = ""
#   encryption_key = "arn:aws:kms:${local.aws_region}:${local.aws_account_id}:alias/aws/s3"
#   name           = "test"
#   queued_timeout = 480
#   service_role   = aws_iam_role.test_codebuild.arn

#   tags = {
#     Terraform = "true"
#   }

#   artifacts {
#     encryption_disabled    = false
#     name                   = "test"
#     override_artifact_name = false
#     packaging              = "NONE"
#     type                   = "CODEPIPELINE"
#   }

#   cache {
#     modes = []
#     type  = "NO_CACHE"
#   }

#   environment {
#     compute_type                = "BUILD_GENERAL1_SMALL"
#     image                       = "aws/codebuild/standard:4.0"
#     image_pull_credentials_type = "CODEBUILD"
#     privileged_mode             = true
#     type                        = "LINUX_CONTAINER"
#   }

#   logs_config {
#     cloudwatch_logs {
#       status = "DISABLED"
#     }

#     s3_logs {
#       encryption_disabled = false
#       status              = "DISABLED"
#     }
#   }

#   source {
#     buildspec           = file("buildspecs/ecr_default.yml")
#     git_clone_depth     = 0
#     insecure_ssl        = false
#     report_build_status = false
#     type                = "CODEPIPELINE"
#   }
# }