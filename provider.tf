terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "pruebasAlejo"

    workspaces {
      name = "terraform-test"
    }
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.5.0"
    }
  }
}
provider "aws" {
  access_key = var.access_key
  secret_key = var.secret_key
  region     = var.AWS_REGION
}