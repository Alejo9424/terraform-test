# resource "aws_codepipeline" "lumen_devops_test" {
#   name     = "lumen-devops-test"
#   role_arn = aws_iam_role.test_codepipeline.arn

#   artifact_store {
#     location = local.aws_codepipeline_artifact_store_s3_bucket_name
#     type     = "S3"
#   }

#   stage {
#     name = "Source"

#     action {
#       name             = "Source"
#       namespace        = "SourceVariables"
#       category         = "Source"
#       owner            = "AWS"
#       provider         = "CodeStarSourceConnection"
#       version          = "1"
#       output_artifacts = ["SourceArtifact"]

#       configuration = {
#         ConnectionArn        = local.aws_codestar_connection_arn
#         FullRepositoryId     = "4l3j4ndr0/devops-prueba-epayco"
#         BranchName           = "master"
#         OutputArtifactFormat = "CODE_ZIP"
#       }
#     }
#   }

#   stage {
#     name = "Build"

#     action {
#       name             = "Build"
#       namespace        = "BuildVariables"
#       category         = "Build"
#       owner            = "AWS"
#       provider         = "CodeBuild"
#       input_artifacts  = ["SourceArtifact"]
#       output_artifacts = ["BuildArtifact"]
#       version          = "1"

#       configuration = {
#         ProjectName = aws_codebuild_project.test.name
#         EnvironmentVariables = jsonencode([
#           {
#             name  = "AWS_DEFAULT_REGION"
#             type  = "PLAINTEXT"
#             value = local.aws_region
#           },
#           {
#             name  = "AWS_ACCOUNT_ID"
#             type  = "PLAINTEXT"
#             value = local.aws_account_id
#           },
#           {
#             name  = "IMAGE_REPO_NAME"
#             type  = "PLAINTEXT"
#             value = aws_ecr_repository.test.name
#           },
#           {
#             name  = "IMAGE_TAG"
#             type  = "PLAINTEXT"
#             value = "pruebas"
#           },
#           {
#             name  = "DOCKERHUB_USERNAME"
#             type  = "PARAMETER_STORE"
#             value = "DOCKERHUB_USER"
#           },
#           {
#             name  = "DOCKERHUB_PASSWORD"
#             type  = "PARAMETER_STORE"
#             value = "DOCKERHUB_PASSWORD"
#           }
#         ])
#       }
#     }
#   }

#   stage {
#     name = "Deploy"

#     action {
#       name            = "Deploy"
#       namespace       = "DeployVariables"
#       category        = "Deploy"
#       owner           = "AWS"
#       provider        = "ECS"
#       input_artifacts = ["BuildArtifact"]
#       version         = "1"

#       configuration = {
#         ClusterName = aws_ecs_cluster.testing.name
#         ServiceName = aws_ecs_service.lumen_prueba.name
#       }
#     }
#   }
# }
