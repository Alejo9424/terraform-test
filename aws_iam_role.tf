# resource "aws_iam_role" "ecs_task_execution" {
#   name = "ecsTaskExecutionRole"
#   path = "/"

#   assume_role_policy = <<POLICY
# {
#     "Version": "2008-10-17",
#     "Statement": [
#         {
#             "Action": "sts:AssumeRole",
#             "Effect": "Allow",
#             "Principal": {
#                 "Service": "ecs-tasks.amazonaws.com"
#             },
#             "Sid": ""
#         }
#     ]
# }
# POLICY

#   tags = {
#     Terraform = "true"
#   }
# }

# resource "aws_iam_role_policy" "ecs_systems_manager_get_parameter" {
#   name = "ECSSystemsManagerGetParameter"
#   role = aws_iam_role.ecs_task_execution.id

#   policy = <<POLICY
# {
#     "Version": "2012-10-17",
#     "Statement": [
#         {
#             "Sid": "",
#             "Effect": "Allow",
#             "Action": [
#                 "ssm:GetParametersByPath",
#                 "ssm:GetParameters",
#                 "ssm:GetParameter"
#             ],
#             "Resource": "arn:aws:ssm:${local.aws_region}:${local.aws_account_id}:parameter/*"
#         }
#     ]
# }
# POLICY
# }

# resource "aws_iam_role" "test_codebuild" {
#   name = "codebuild-test-service-role"
#   path = "/service-role/"

#   assume_role_policy = file("policies/codebuild_role.json")

#   tags = {
#     Terraform = "true"
#   }
# }

# resource "aws_iam_policy" "test_codebuild" {
#   name        = "CodeBuildBasePolicy-test-${local.aws_region}"
#   description = "Policy used in trust relationship with CodeBuild"
#   path        = "/service-role/"

#   policy = templatefile("policies/codebuild.tmpl.json", {
#     project_name = "test"
#     region       = local.aws_region
#     account_id   = local.aws_account_id
#   })
# }

# resource "aws_iam_role_policy_attachment" "test_codebuild" {
#   role       = aws_iam_role.test_codebuild.name
#   policy_arn = aws_iam_policy.test_codebuild.arn
# }

# resource "aws_iam_role" "test_codepipeline" {
#   name = "AWSCodePipelineServiceRole-${local.aws_region}-test"
#   path = "/service-role/"

#   assume_role_policy = file("policies/codepipeline_assume_role.json")

#   tags = {
#     Terraform = "true"
#   }
# }

# resource "aws_iam_policy" "test_codepipeline" {
#   name        = "AWSCodePipelineServiceRole-${local.aws_region}-test"
#   description = "Policy used in trust relationship with CodePipeline"
#   path        = "/service-role/"
#   policy      = file("policies/codepipeline.json")
# }